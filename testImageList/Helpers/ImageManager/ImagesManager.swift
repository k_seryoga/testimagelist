//
//  ImagesManager.swift
//  testImageList
//
//  Created by Serhii Kostiuk on 10/17/19.
//  Copyright © 2019 Serhii Kostiuk. All rights reserved.
//

import Foundation
import UIKit

protocol ImagesManagerOutputProtocol: class {
    func tagResult()
}

class ImagesManager {
    // MARK: - Public Types
    
    typealias downloadCompletion = (_ image: UIImage) -> Void
    
    // MARK: - Public Static Properties

    static let sharedInstance = ImagesManager()
    
    // MARK: - Public Properties
    weak var delegate: ImagesManagerOutputProtocol?
    private(set) var imageModels = [ImageModel]()

    // MARK: - Private Properties

    private let imageCache = NSCache<NSString, UIImage>()

    // MARK: - Private Init

    private init() {}
    
    // MARK: - Public Properties

    func getImages(tag: String) {
        NetworkManager.sharedInstance.search(text: tag) { (success, json, error) in
            guard let dataArray = json?.value(forKeyPath: "response.photos") as? NSArray else { return }
            
            if !self.imageModels.isEmpty {
                self.imageModels.removeAll()
                self.imageCache.removeAllObjects()
            }
            
            self.parse(data: dataArray)
            self.delegate?.tagResult()
        }
    }
    
    func downloadImage(imageUrlString: String, downloadCompletion: downloadCompletion?) {
        guard let url = URL(string:imageUrlString) else {
            return
        }
        
        if let cachedImage = imageCache.object(forKey: imageUrlString as NSString) {
            downloadCompletion?(cachedImage)
        } else {
            NetworkManager.sharedInstance.downloadImage(from: url) { (success, data, error) in
                guard error == nil, let imageData = data, let image = UIImage(data: imageData) else { return }
                
                self.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                downloadCompletion?(image)
            }
        }
        
    }
    
    
    private func parse(data: NSArray) {
        data.forEach { (photosArray) in
            guard let photoArray = photosArray as? NSArray else { return }
            
            for dictionary in photoArray {
                guard let sizeDic = dictionary as? [String: Any] else { return }
                decodeModel(data: sizeDic["original_size"] as Any)
            }
        }
    }
    
    private func decodeModel(data: Any) {
        if let theJSONData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted) {
            do {
                let decoder = JSONDecoder()
                let imageModel = try decoder.decode(ImageModel.self, from: theJSONData)
                self.imageModels.append(imageModel)
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}
