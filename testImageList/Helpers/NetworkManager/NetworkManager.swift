//
//  NetworkManager.swift
//  testImageList
//
//  Created by Serhii Kostiuk on 10/16/19.
//  Copyright © 2019 Serhii Kostiuk. All rights reserved.
//

import Foundation

class NetworkManager {
    
    // MARK: - Public Types
    
    typealias searchCompletion = (_ success: Bool, _ json: NSDictionary?, _ error: Error?) -> Void
    typealias downloadCompletion = (_ success: Bool, _ data: Data?, _ error: Error?) -> Void

    // MARK: - Public Static Properties

    static let sharedInstance = NetworkManager()

    // MARK: - Private Static Properties

    private let baseUrl = "https://api.tumblr.com/v2/"
    private let apiKey = "CcEqqSrYdQ5qTHFWssSMof4tPZ89sfx6AXYNQ4eoXHMgPJE03U"
    
    // MARK: - Private Types

    private typealias requestCompletion = (_ success: Bool, _ data: Data?, _ error: Error?) -> Void

    // MARK: - Private Init

    private init() {}
    
    // MARK: - Public Methods

    func search(text: String, searchCompletion: searchCompletion?) {
        guard let url = URL(string:"\(baseUrl)tagged?tag=\(text)&api_key=\(apiKey)") else { return }
        let urlRequest = URLRequest(url: url)
        
        request(urlRequest) { (success, data, error) in
            guard let data = data else {
                searchCompletion?(false, nil, error)
                return
            }
            
            let theJSONData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary
            guard let json = theJSONData else {
                searchCompletion?(false, nil, error)
                return
            }
            
            searchCompletion?(true, json, error)
        }
    }
    
    func downloadImage(from url: URL, downloadCompletion: downloadCompletion?) {
        let urlRequest = URLRequest(url: url)
        request(urlRequest) { (success, data, error) in
            downloadCompletion?(success, data, error)
        }
    }
    
    // MARK: - Private Methods

    private func request(_ urlRequest: URLRequest, requestCompletion: requestCompletion?) {
       let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
         guard let data = data, let urlResponse = response as? HTTPURLResponse else {
           print(error?.localizedDescription ?? "")
           requestCompletion?(false, nil, error)
           return
         }
         
         if urlResponse.statusCode != 200 {
           print(error?.localizedDescription ?? "")
           requestCompletion?(false, nil, error)
         } else {
           requestCompletion?(true, data, error)
         }
       }
       
       task.resume()
     }
    
}
