//
//  ImageDetailViewController.swift
//  testImageList
//
//  Created by Serhii Kostiuk on 10/16/19.
//  Copyright © 2019 Serhii Kostiuk. All rights reserved.
//

import UIKit

class ImageDetailViewController: UIViewController {
    // MARK: - Public Properties

    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var activityIndicatorBackgroundView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Private Properties

    private var model: ImageModel?
    private let imageCache = NSCache<NSString, UIImage>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let imageModel = model else { return }
        ImagesManager.sharedInstance.downloadImage(imageUrlString: imageModel.urlString) { (image) in
            if Thread.isMainThread {
                self.configure(image: image)
            } else {
                DispatchQueue.main.async {
                    self.configure(image: image)
                }
            }
        }

    }

    func setup(model: ImageModel) {
      self.model = model
    }
    
    private func configure(image: UIImage) {
        self.contentImageView.image = image
        self.activityIndicatorBackgroundView.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
}

