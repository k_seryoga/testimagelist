//
//  ImagesListTableViewCell.swift
//  testImageList
//
//  Created by Serhii Kostiuk on 10/17/19.
//  Copyright © 2019 Serhii Kostiuk. All rights reserved.
//

import UIKit

class ImagesListTableViewCell: UITableViewCell {
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var activityIndicatorBackgroundView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        contentImageView.image = nil
    }
    
    func showLoading(_ show: Bool) {
        if show {
            activityIndicator.startAnimating()
//            self.activityIndicatorBackgroundView.isHidden = false
        } else {
            activityIndicator.stopAnimating()
        }
        
    }
}
