//
//  ImagesListViewController.swift
//  testImageList
//
//  Created by Serhii Kostiuk on 10/16/19.
//  Copyright © 2019 Serhii Kostiuk. All rights reserved.
//

import UIKit

class ImagesListViewController: UIViewController {
    // MARK: - Private Properties

    @IBOutlet private weak var imagesListTableView: UITableView!
    @IBOutlet private weak var tagSearchBar: UISearchBar!
    @IBOutlet private weak var introView: UIView!
        
    // MARK: - Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        ImagesManager.sharedInstance.delegate = self
    }
    
    // MARK: - Private Methods

    private func showImage(_ imageModel: ImageModel) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: String(describing: ImageDetailViewController.self)) as? ImageDetailViewController else {
            return
        }
        
        navigationController?.pushViewController(viewController, animated: true)
        viewController.setup(model: imageModel)
    }
}

extension ImagesListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ImagesManager.sharedInstance.imageModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: String(describing: ImagesListTableViewCell.self), for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let imageCell = cell as? ImagesListTableViewCell else { return }
        let imageModel = ImagesManager.sharedInstance.imageModels[indexPath.item]
        ImagesManager.sharedInstance.downloadImage(imageUrlString: imageModel.urlString) { (image) in
            if Thread.isMainThread {
                self.configure(cell: imageCell, image: image)
            } else {
                DispatchQueue.main.async {
                    self.configure(cell: imageCell, image: image)
                }
            }
        }
    }
    
 
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showImage(ImagesManager.sharedInstance.imageModels[indexPath.item])
    }
    
    // MARK: - Private Methods

    private func configure(cell: ImagesListTableViewCell, image: UIImage) {
        cell.contentImageView.image = UIImage.resizeImage(image: image, newWidth: 300)
        cell.activityIndicatorBackgroundView.isHidden = true
        cell.activityIndicator.stopAnimating()
    }
}


extension ImagesListViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        guard let tagText = searchBar.text else { return }
        showIntroView(show: !tagText.isEmpty)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        guard let tagText = searchBar.text else { return }
        showIntroView(show: !tagText.isEmpty)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let tagText = searchBar.text else { return }
        
        showIntroView(show: !tagText.isEmpty)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let tagText = searchBar.text else { return }
        if !tagText.isEmpty {
            showIntroView(show: true)
            ImagesManager.sharedInstance.getImages(tag:tagText)
            searchBar.resignFirstResponder()
        }
    }
    
    // MARK: - Private Methods

    private func showIntroView(show: Bool) {
        introView.isHidden = show
    }
}

extension ImagesListViewController: ImagesManagerOutputProtocol {
    func tagResult() {
        DispatchQueue.main.async {
            self.imagesListTableView.reloadData()
        }
    }
}
