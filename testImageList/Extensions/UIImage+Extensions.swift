//
//  UIImage+Extensions.swift
//  testImageList
//
//  Created by Serhii Kostiuk on 10/17/19.
//  Copyright © 2019 Serhii Kostiuk. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
   class func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {

        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
}
