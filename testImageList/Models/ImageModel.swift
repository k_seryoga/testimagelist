//
//  ImageModel.swift
//  testImageList
//
//  Created by Serhii Kostiuk on 10/17/19.
//  Copyright © 2019 Serhii Kostiuk. All rights reserved.
//

import Foundation

struct ImageModel: Codable {
    var urlString: String
    
    enum CodingKeys: String, CodingKey {
        case urlString = "url"
    }
}


